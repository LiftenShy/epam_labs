﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Data;
using Tickets.Data.Models;
using Xunit;

namespace Ticket.Test
{
    public class DataTest
    {
        [Fact]
        public void CityTestOnCreateUpdateInsertDelete()
        {
            using (var cityRepository = new EfRepository<City>())
            {
                 City city = new City {Name = "TestName"};
                 cityRepository.Insert(city);
                 Assert.True(city.Id > 0);

                string changeName = city.Name;
                 city.Name = "name test";
                 cityRepository.Update(city);
                 Assert.Equal(changeName,city.Name);

                 City newCity = cityRepository.Table.FirstOrDefault(c => c.Id == city.Id);
                 Assert.NotNull(newCity);
                 Assert.Equal(newCity.Id, city.Id);
                Assert.Equal(city.Name, newCity.Name);

                 cityRepository.Delete(city);
                 City newUserAfterDelete = cityRepository.Table.FirstOrDefault(c => c.Id == city.Id);
                 Assert.NotNull(newUserAfterDelete);
            }
        }
    }
}
