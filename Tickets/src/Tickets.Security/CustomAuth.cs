﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Tickets.Business.Abstract;
using Tickets.Data.Models;

namespace Tickets.Security
{
    public class CustomAuth : IAuthentication
    {
        private IUserService UserService { get;}
        private IHttpContextAccessor _contextAccessor;

        public CustomAuth(IUserService userService, IHttpContextAccessor contextAccessor)
        {
            UserService = userService;
            this._contextAccessor = contextAccessor;
        }

        public void SignIn(UserAccount userAccount)
        {
            if (UserService.ValidateUser(userAccount))
            {
                Authentication(UserService.GetUserAccountByName(userAccount.UserName));
            }
            else
            {
                throw new ValidationException("You enter wrong user name or password");
            }
        }

        public void Registration(UserAccount userAccount)
        {
            UserService.CreateUser(userAccount);
            Authentication(UserService.GetUserAccountByName(userAccount.UserName));
        }

        public void Authentication(UserAccount userAccount)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, userAccount.UserName),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, userAccount.Role.NameRole)
                };
            
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            _contextAccessor.HttpContext.Authentication.SignInAsync("MyCookies", new ClaimsPrincipal(id));
        }

        public void Dispose()
        {
            _contextAccessor = null;
        }
    }
}