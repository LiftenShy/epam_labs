﻿using System;
using System.Threading.Tasks;
using Tickets.Data.Models;

namespace Tickets.Security
{
    public interface IAuthentication : IDisposable
    {
        void SignIn(UserAccount userAccount);
        void Registration(UserAccount userAccount);
    }
}
