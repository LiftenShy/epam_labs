﻿using Autofac;
using Tickets.Business;
using Tickets.Business.Abstract;
using Tickets.Data;
using Tickets.Security;
using Tickets.Storage;

namespace Tickets.DependencyResolver
{
    public class DefaultModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<CustomAuth>().As<IAuthentication>();
            builder.RegisterType<EventService>().As<IEventsService>();
            builder.RegisterType<TicketsService>().As<ITicketsService>();
            builder.RegisterType<CityService>().As<ICityService>();
            builder.RegisterType<VenueService>().As<IVenueService>();
            builder.RegisterType<OrderService>().As<IOrderService>();
            builder.RegisterType<ImageService>().As<IImageService>();
            builder.RegisterType<LocalFileManager>().As<IFileManager>();
        }
    }
}
