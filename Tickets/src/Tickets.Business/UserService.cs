﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Tickets.Business.Abstract;
using Tickets.Data;
using Tickets.Data.Models;

namespace Tickets.Business
{
    public class UserService : IUserService
    {
        private readonly IRepository<UserAccount> _userAccountRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Role> _roleRepository;

        public UserService(IRepository<UserAccount> userAccountRepository, IRepository<User> userRepository,
            IRepository<Role> roleRepository)
        {
            _userAccountRepository = userAccountRepository;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }

        public void CreateUser(UserAccount userAccount)
        {
            var userAccountList = _userAccountRepository.Table.ToList();

            foreach (UserAccount item in userAccountList)
            {
                if (item.UserName == userAccount.UserName)
                {
                    throw new ValidationException("this user name already have, choose another name");
                }
            }
            _userRepository.Insert(new User {Address = "", FirstName = "", LastName = "", Localization = "en"});
            _userAccountRepository.Insert(new UserAccount
            {
                UserName = userAccount.UserName,
                Password = userAccount.Password,
                RoleId = userAccount.RoleId,
                UserId = _userRepository.Table.Last().Id
            });

        }

        public bool ValidateUser(UserAccount userAccount)
        {
            foreach (UserAccount item in _userAccountRepository.Table.ToList())
            {
                if (item.UserName.Equals(userAccount.UserName) && item.Password.Equals(userAccount.Password))
                {
                    return true;
                }
            }
            return false;
        }

        public UserAccount GetUserAccountByid(int id)
        {
            return _userAccountRepository.Table.Include(r => r.Role).FirstOrDefault(ua => ua.Id == id);
        }

        public User GetUserById(int id)
        {
            return _userRepository.Table.FirstOrDefault(u => u.Id == id);
        }

        public List<UserAccount> GetAllUserAccounts()
        {
            return _userAccountRepository.Table.Include(r => r.Role).ToList();
        }

        public UserAccount GetUserAccountByName(string userName)
        {
            return _userAccountRepository.Table.Include(r => r.Role).FirstOrDefault(ua => ua.UserName.Equals(userName));
        }

        public User GetUserByName(string userName)
        {
            return
                _userRepository.Table.Include(ua => ua.UserAccount)
                    .FirstOrDefault(u => u.UserAccount.UserName.Equals(userName));
        }

        public void ChangeRoleByUser(string nameRole, int id)
        {
            UserAccount newUa = _userAccountRepository.Table.FirstOrDefault(ua => ua.Id == id);
            newUa.RoleId = _roleRepository.Table.FirstOrDefault(r => r.NameRole.Equals(nameRole)).Id;
            _userAccountRepository.Update(newUa);
        }

        public void EditUserProfile(User userInfo)
        {
            _userRepository.Update(userInfo);
        }
    }
}
