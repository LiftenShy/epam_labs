﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Tickets.Business.Abstract;
using Tickets.Data;
using Tickets.Data.Models;

namespace Tickets.Business
{
    public class VenueService : IVenueService
    {
        private IRepository<Venue> _venueRepository;

        public VenueService(IRepository<Venue> venueRepository)
        {
            _venueRepository = venueRepository;
        }

        public List<Venue> GetAllVenue()
        {
            return _venueRepository.Table.Include(v => v.City).ToList();
        }

        public Venue GetVenueById(int id)
        {
            return _venueRepository.Table.Include(v => v.City).FirstOrDefault(v => v.Id == id);
        }

        public Venue GetVenueByName(string name)
        {
            return _venueRepository.Table.Include(v => v.City).FirstOrDefault(v => v.Name.Equals(name));
        }

        public void AddVenue(Venue venue)
        {
            if (!_venueRepository.Table.Any(v => v.Name.Equals(venue.Name)))
            {
                _venueRepository.Insert(venue);
            }
            else
            {
                throw new ValidationException("Enter your name venue already existing");
            }
        }
    }
}
