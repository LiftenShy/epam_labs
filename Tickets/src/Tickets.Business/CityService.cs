﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Tickets.Business.Abstract;
using Tickets.Data;
using Tickets.Data.Models;

namespace Tickets.Business
{
    public class CityService : ICityService
    {
        private readonly IRepository<City> _cityRepository;

        public CityService(IRepository<City> cityRepository)
        {
            _cityRepository = cityRepository;
        }

        public List<City> GetAllCity()
        {
            return _cityRepository.Table.Include(c => c.Venues).ToList();
        }

        public City GetCityById(int id)
        {
            return _cityRepository.Table.FirstOrDefault(c => c.Id == id);
        }

        public void AddCity(string name)
        {
            if (!_cityRepository.Table.Any(c => c.Name.Equals(name)))
            {
                _cityRepository.Insert(new City {Name = name});
            }
            else
            {
                throw new ValidationException("Enter your name existing");
            }
        }

        public City GetCityByName(string name)
        {
            return _cityRepository.Table.FirstOrDefault(c => c.Name.Equals(name));
        }
    }
}
