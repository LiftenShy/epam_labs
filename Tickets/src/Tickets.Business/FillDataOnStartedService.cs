﻿using System.Linq;
using Tickets.Data;

namespace Tickets.Business
{
    public class FillDataOnStartedService
    {
        private static OnStartAppFirstData _data = new OnStartAppFirstData();
        private static TicketContext _context = new TicketContext();

        public static void AddData()
        {

            if (!_context.Roles.Any())
            {
                _context.AddRange(_data._listCity);
                _context.SaveChanges();
                _context.AddRange(_data._listVenue);
                _context.SaveChanges();
                _context.AddRange(_data._listEvents);
                _context.SaveChanges();
                _context.AddRange(_data._listRoles);
                _context.SaveChanges();
                _context.AddRange(_data._listUsers);
                _context.SaveChanges();
                _context.AddRange(_data._listAccountUsers);
                _context.SaveChanges();
                _context.AddRange(_data._listTickets);
                _context.SaveChanges();
                _context.AddRange(_data._listOrders);
                _context.SaveChanges();
            }
        }
    }
}
