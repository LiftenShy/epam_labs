using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Tickets.Business.Abstract;
using Tickets.Data;
using Tickets.Data.Models;

namespace Tickets.Business
{
    public class OrderService : IOrderService
    {
        private IRepository<Order> _orderRepository;

        public OrderService(IRepository<Order> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Order GetOrderById(int id)
        {
            return _orderRepository.Table.Include(t => t.Ticket).FirstOrDefault(o => o.Id == id);
        }

        public List<Order> GetOrdersById(int id)
        {
            return
                _orderRepository.Table.Include(t => t.Ticket)
                    .Include(e => e.Ticket.Event)
                    .Where(ord => ord.BuyerId == id)
                    .ToList();
        }

        public List<Order> GetOrdersByName(string name)
        {
            return
                _orderRepository.Table.Include(t => t.Ticket)
                    .Include(e => e.Ticket.Event)
                    .Where(ord => ord.NameBuyer.Equals(name))
                    .ToList();
        }

        public void SetOrderToUser(Order order, UserAccount user)
        {
            order.BuyerId = user.Id;
            order.NameBuyer = user.UserName;
            order.Status = "Waiting for confirmation";
            _orderRepository.Update(order);
        }

        public void Accept(Order order)
        {
            order.Status = "Confirmed";
            _orderRepository.Update(order);
        }

        public void Reject(Order order, string description)
        {
            order.Status = "Reject";
            order.Description = description;
            _orderRepository.Update(order);
        }
    }
}
