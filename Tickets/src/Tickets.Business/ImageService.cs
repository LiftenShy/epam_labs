﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Business.Abstract;
using Tickets.Storage;

namespace Tickets.Business
{
    public class ImageService : IImageService
    {
        private readonly IFileManager _fileManager;

        public ImageService(IFileManager fileManager)
        {
            this._fileManager = fileManager;
        }

        public string GetImageUri(string fileName)
        {
            return _fileManager.GetURI(fileName);
        }

        public string SaveImage(string fileName, Stream stream)
        {
            _fileManager.SaveFile(fileName,stream);
            return fileName;
        }
    }
}
