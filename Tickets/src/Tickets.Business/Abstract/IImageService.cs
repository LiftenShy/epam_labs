﻿
using System.IO;

namespace Tickets.Business.Abstract
{
    public interface IImageService : IServiceBase
    {
        string GetImageUri(string fileName);
        string SaveImage(string fileName, Stream stream);
    }
}
