﻿
using System.Collections.Generic;
using Tickets.Data.Models;

namespace Tickets.Business.Abstract
{
    public interface ICityService : IServiceBase
    {
        List<City> GetAllCity();
        City GetCityById(int id);
        void AddCity(string name);
        City GetCityByName(string name);
    }
}
