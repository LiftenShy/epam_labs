﻿

using System.Collections.Generic;
using Tickets.Data.Models;

namespace Tickets.Business.Abstract
{
    public interface IEventsService : IServiceBase
    {
        List<Event> GetAllEvents();
        Event GetEventByName(string eventName);
        void AddEvent(Event ev);
        void UpdateEvent(Event ev);
        void RemoveEvent(int id);
        Event GetEventById(int id);
    }
}
