﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Data.Models;

namespace Tickets.Business.Abstract
{
    public interface ITicketsService
    {
        List<Ticket> GetTicketsByIdEvent(int id);
        List<Ticket> GetTicketsByUserName(string userName);
        Ticket GetTicketById(int id);
        void BuyTicket(Ticket ticket);
        void Accept(Ticket ticket);
        void Reject(Ticket ticket);
        void AddTicket(Ticket ticket);
    }
}
