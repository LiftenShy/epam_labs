
using System;
using System.Collections.Generic;
using Tickets.Data.Models;

namespace Tickets.Business.Abstract
{
    public interface IUserService : IServiceBase
    {
        UserAccount GetUserAccountByid(int id);
        List<UserAccount> GetAllUserAccounts();
        void CreateUser(UserAccount userAccount);
        bool ValidateUser(UserAccount userAccount);
        UserAccount GetUserAccountByName(string userName);
        User GetUserByName(string userName);
        void ChangeRoleByUser(string name, int id);
        void EditUserProfile(User userInfo);
        User GetUserById(int id);
    }
}