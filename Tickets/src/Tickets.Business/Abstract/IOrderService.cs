﻿
using System.Collections.Generic;
using Tickets.Data.Models;

namespace Tickets.Business.Abstract
{
    public interface IOrderService : IServiceBase
    {
        List<Order> GetOrdersById(int id);
        Order GetOrderById(int id);
        List<Order> GetOrdersByName(string name);
        void SetOrderToUser(Order order, UserAccount user);
        void Accept(Order order);
        void Reject(Order order, string description);
    }
}
