﻿using System.Collections.Generic;
using Tickets.Data.Models;

namespace Tickets.Business.Abstract
{
    public interface IVenueService : IServiceBase
    {
        List<Venue> GetAllVenue();
        Venue GetVenueById(int id);
        Venue GetVenueByName(string name);
        void AddVenue(Venue venue);
    }
}
