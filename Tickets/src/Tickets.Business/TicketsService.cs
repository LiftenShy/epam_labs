﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Tickets.Business.Abstract;
using Tickets.Data;
using Tickets.Data.Models;

namespace Tickets.Business
{
    public class TicketsService : ITicketsService
    {
        private readonly IRepository<Ticket> _ticketRepository;
        private readonly IRepository<Order> _orderRepository;

        public TicketsService(IRepository<Ticket> ticketRepository, IRepository<Order> orderRepository)
        {
            _ticketRepository = ticketRepository;
            _orderRepository = orderRepository;
        }

        public List<Ticket> GetTicketsByIdEvent(int id)
        {
            return _ticketRepository.Table.Include(e => e.Event).Include(o => o.Order).Where(t => t.Event.Id == id).ToList();
        }

        public List<Ticket> GetTicketsByUserName(string userName)
        {
            return _ticketRepository.Table.Include(o => o.Order).Include(e => e.Event).Where(t => t.Seller.Equals(userName)).ToList();
        }

        public Ticket GetTicketById(int id)
        {
            return _ticketRepository.Table.Include(o => o.Order).Include(e => e.Event).FirstOrDefault(t => t.Id == id);
        }

        public void AddTicket(Ticket ticket)
        {
            _ticketRepository.Insert(ticket);
            _orderRepository.Insert(new Order {TicketId = _ticketRepository.Table.LastOrDefault().Id});
        }

        public void BuyTicket(Ticket ticket)
        {
            ticket.Status = "Waiting Confirmation";
            _ticketRepository.Update(ticket);
        }

        public void Accept(Ticket ticket)
        {
            ticket.Status = "Sold";
            _ticketRepository.Update(ticket);
        }

        public void Reject(Ticket ticket)
        {
            ticket.Status = "Selling";
            _ticketRepository.Update(ticket);
        }
    }
}
