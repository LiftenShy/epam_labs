﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Tickets.Business.Abstract;
using Tickets.Data;
using Tickets.Data.Models;
using System.Linq;

namespace Tickets.Business
{
    public class EventService : IEventsService
    {
        private readonly IRepository<Event> _repositroyEvent;

        public EventService(IRepository<Event> repositoryEvent)
        {
            _repositroyEvent = repositoryEvent;
        }

        public List<Event> GetAllEvents()
        {
            return _repositroyEvent
                .Table.Include(v => v.Venue)
                .Include(c => c.Venue.City).ToList();
        }

        public Event GetEventById(int id)
        {
            return
                _repositroyEvent.Table
                    .Include(v => v.Venue)
                    .Include(c => c.Venue.City)
                    .FirstOrDefault(e => e.Id == id);
        }

        public Event GetEventByName(string eventName)
        {
            return
                _repositroyEvent.Table.Include(v => v.Venue)
                    .Include(c => c.Venue.City)
                    .FirstOrDefault(e => e.Name == eventName);
        }

        public void AddEvent(Event ev)
        {
            if (!_repositroyEvent.Table.Any(e => e.Name == ev.Name))
            {
                _repositroyEvent.Insert(ev);
            }
            else
            {
                throw new ValidationException("this name already have, choose another name");
            }
        }

        public void UpdateEvent(Event ev)
        {
            _repositroyEvent.Update(ev);
        }

        public void RemoveEvent(int id)
        {
            _repositroyEvent.Delete(GetEventById(id));
        }
    }
}
