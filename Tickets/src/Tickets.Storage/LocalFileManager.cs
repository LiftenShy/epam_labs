﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Tickets.Storage
{
    public class LocalFileManager : IFileManager
    {
        private readonly string _basePath;

        public LocalFileManager(IHostingEnvironment hostingEnvironment)
        {
            _basePath = hostingEnvironment.WebRootPath + @"\Images\";
        }

        public void SaveFile(String fileName, Stream content)
        {
            using (FileStream fileStream = new FileStream(_basePath + fileName, FileMode.Create))
            {
                content.CopyTo(fileStream);
            }
        }

        public String GetURI(string fileName)
        {
            return @"~/Images/"+fileName;
        }
    }
}
