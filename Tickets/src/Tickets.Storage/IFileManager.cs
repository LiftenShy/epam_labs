﻿using System;
using System.IO;

namespace Tickets.Storage
{
    public interface IFileManager
    {
        void SaveFile(String fileName, Stream content);
        String GetURI(string fileName);
    }
}