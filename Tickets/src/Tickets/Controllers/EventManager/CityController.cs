﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using Tickets.Business.Abstract;
using Tickets.Models;

namespace Tickets.Controllers.EventManager
{
    [Authorize(Roles = "admin")]
    public class CityController : Controller
    {
        private readonly ICityService _cityService;
        private readonly IStringLocalizer<BaseLocalizationController> _stringLocalizer;

        public CityController(ICityService cityService, IStringLocalizer<BaseLocalizationController> stringLocalizer)
        {
            _cityService = cityService;
            _stringLocalizer = stringLocalizer;
        }
        public ActionResult City()
        {
            return View("../EventManager/CreateCity");
        }

        [HttpPost]
        public ActionResult AddCity(CityModel city)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _cityService.AddCity(city.Name);
                    return
                        RedirectToRoute(
                            new
                            {
                                controller = "Venue",
                                action = "Venue",
                                culture = HttpContext.Session.GetString("culture")
                            });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", _stringLocalizer[e.Message]);
                    return View("CreateCity", city.Name);
                }
            }
            ModelState.AddModelError("","Enter your name uncorect");
            return View("CreateCity", city.Name);
        }
    }
}
