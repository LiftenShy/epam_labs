﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tickets.Business.Abstract;
using Tickets.Data.Models;
using Tickets.Models;

namespace Tickets.Controllers.EventManager
{
    [Authorize(Roles = "admin")]
    public class EventManagerController : Controller
    {
        private IEventsService _eventService;
        private IVenueService _venueService;
        private ICityService _cityService;
        private IImageService _imageService;

        public EventManagerController(IEventsService eventsService, IVenueService venueService,
            ICityService cityService, IImageService imageService)
        {
            _eventService = eventsService;
            _venueService = venueService;
            _cityService = cityService;
            _imageService = imageService;
        }

        [Authorize(Roles = "admin")]
        public ActionResult GetAllEvent()
        {
            return View("EventManager", new EventModel {ListEvents = _eventService.GetAllEvents()});
        }

        [Authorize(Roles = "admin")]
        public ActionResult EditEvent(int id)
        {
            return View("CreateOrUpdateEvent", new EventModel
            {
                Event = _eventService.GetEventById(id),
                ListCity = _cityService.GetAllCity(),
                ListVenue = _venueService.GetAllVenue()
            });
        }


        public ActionResult EditEventPost(EventModel eventModel)
        {
            if (ModelState.IsValid)
            {
                Event ev = _eventService.GetEventByName(eventModel.Name);
                ev.Name = eventModel.Name;
                ev.Date = eventModel.Date;
                if (eventModel.Image != null)
                {
                    ev.PathBanner =
                        _imageService.GetImageUri(_imageService.SaveImage(eventModel.Image.FileName,
                            eventModel.Image.OpenReadStream()));
                }
                ev.Description = eventModel.Description;
                ev.VenueId = _venueService.GetVenueByName(eventModel.VenueName).Id;
                _eventService.UpdateEvent(ev);
                return RedirectToAction("GetAllEvent");
            }
            return View("CreateOrUpdateEvent", eventModel);
        }

        public ActionResult RemoveEvent(int id)
        {
            if (ModelState.IsValid)
            {
                _eventService.RemoveEvent(id);
                return RedirectToAction("GetAllEvent");
            }
            ModelState.AddModelError("", "Choose value not correct");
            return LocalRedirect("");
        }

        [HttpGet]
        public ActionResult CreateEvent()
        {
            return View("CreateOrUpdateEvent", new EventModel
            {
                ListCity = _cityService.GetAllCity(),
                ListVenue = _venueService.GetAllVenue()
            });
        }

        [HttpPost]
        public ActionResult CreateEvent(EventModel eventModel)
        {
            if (ModelState.IsValid)
            {
                _eventService.AddEvent(new Event
                {
                    Name = eventModel.Name,
                    Date = eventModel.Date,
                    PathBanner =
                        _imageService.GetImageUri(_imageService.SaveImage(eventModel.Image.FileName,
                            eventModel.Image.OpenReadStream())),
                    Description = eventModel.Description,
                    VenueId = _venueService.GetVenueByName(eventModel.VenueName).Id
                });
                return RedirectToAction("GetAllEvent");
            }
            return View("CreateOrUpdateEvent", eventModel);
        }
    }
}
