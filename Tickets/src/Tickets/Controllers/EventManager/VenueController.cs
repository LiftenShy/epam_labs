﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Tickets.Business.Abstract;
using Tickets.Data.Models;
using Tickets.Models;

namespace Tickets.Controllers.EventManager
{
    [Authorize(Roles = "admin")]
    public class VenueController : Controller
    {
        private readonly IVenueService _venueService;
        private readonly ICityService _cityService;
        private readonly IStringLocalizer<BaseLocalizationController> _stringLocalizer;

        public VenueController(IVenueService venueService, ICityService cityService,
            IStringLocalizer<BaseLocalizationController> stringLocalizer)
        {
            _venueService = venueService;
            _cityService = cityService;
            _stringLocalizer = stringLocalizer;
        }
        
        public ActionResult Venue()
        {
            return View("../EventManager/CreateVenue", new VenueModel {ListCity = _cityService.GetAllCity()});
        }

        [HttpPost]
        public ActionResult AddVenue(VenueModel venue)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _venueService.AddVenue(new Venue
                    {
                        Name = venue.Name,
                        Adress = venue.Adress,
                        CityId = _cityService.GetCityByName(venue.NameCity).Id
                    });

                    return
                        RedirectToRoute(
                            new
                            {
                                controller = "EventManager",
                                action = "GetAllEvent",
                                culture = HttpContext.Session.GetString("culture")
                            });
                }
                catch (ValidationException e)
                {
                    ModelState.AddModelError("", _stringLocalizer[e.Message]);
                    return View("CreateVenue", venue);
                }
            }
            ModelState.AddModelError("", "Enter your data uncorect");
            return View("CreateVenue", venue);
        }
    }
}
