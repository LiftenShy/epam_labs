﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Localization;
using Tickets.Business.Abstract;
using Tickets.Data.Models;
using Tickets.Models;
using Microsoft.AspNetCore.Http;

namespace Tickets.Controllers
{
    public class UserInfoController : Controller
    {
        private readonly IUserService _userService;
        private readonly IStringLocalizer<BaseLocalizationController> _stringLocalizer;

        public UserInfoController(IUserService userService, IStringLocalizer<BaseLocalizationController> stringLocalizer)
        {
            _userService = userService;
            _stringLocalizer = stringLocalizer;
        }

        public ActionResult UserInfo()
        {
            return View("UserInfo", new UserInfoModel {User = _userService.GetUserByName(User.Identity.Name)});
        }

        public ActionResult EditUserInfo(int id)
        {
            return View("EditUserInfo", new UserInfoModel { User = _userService.GetUserById(id)});
        }

        public ActionResult EditUserInfoPost(UserInfoModel userInfo)
        {
            if (ModelState.IsValid)
            {
                _userService.EditUserProfile(new User
                {
                    FirstName = userInfo.FirstName,
                    LastName = userInfo.LastName,
                    Address = userInfo.Address,
                    Localization = userInfo.Localization,
                    PhoneNumber = userInfo.PhoneNumber
                });
                return
                    RedirectToRoute(
                        new
                        {
                            controller = "UserInfo",
                            action = "UserInfo",
                            culture = HttpContext.Session.GetString("culture")
                        });
            }
            ModelState.AddModelError("", "Enter your data uncorect");
            return View("EditUserInfo", userInfo);
        }
    }
}
