﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Tickets.Data.Models;
using Tickets.Models;
using Tickets.Security;

namespace Tickets.Controllers.Account
{
    public class LoginController : Controller
    {
        private IAuthentication Authentication { get; }
        public IStringLocalizer<BaseLocalizationController> StringLocalizer { get; set; }

        public LoginController(IAuthentication authentication, IStringLocalizer<BaseLocalizationController> stringLocalizer)
        {
            Authentication = authentication;
            StringLocalizer = stringLocalizer;
        }


        [HttpGet]
        public ActionResult Login()
        {
            return View("Login");
        }

        [HttpPost]
        public ActionResult PostLogin(UsersModel user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Authentication.SignIn(new UserAccount {UserName = user.UserName, Password = user.Password});
                    return
                        RedirectToRoute(
                            new
                            {
                                controller = "Events",
                                action = "Events",
                                culture = HttpContext.Session.GetString("culture")
                            });
                }
                catch (ValidationException e)
                {
                    ModelState.AddModelError("", StringLocalizer[e.Message]);
                    return View("Login", user);
                }
            }
            ModelState.AddModelError("WrongAccount", StringLocalizer["You enter incorrect symbol"]);
            return View("Login", user);
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return View("Registration");
        }

        [HttpPost]
        public ActionResult PostRegistration(UsersModel user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Authentication.Registration(new UserAccount
                    {
                        UserName = user.UserName,
                        Password = user.Password,
                        RoleId = (int)UsersModel.Role.User
                    });
                    return
                        RedirectToRoute(
                            new
                            {
                                controller = "Events",
                                action = "Events",
                                culture = HttpContext.Session.GetString("culture")
                            });
                }
                catch (ValidationException e)
                {
                    ModelState.AddModelError("WrongRegistration", StringLocalizer[e.Message]);
                    return View("Registration", user);
                }
            }
            ModelState.AddModelError("WrongRegistration", StringLocalizer["You Enter incorrect symbols"]);
            return View("Registration", user);
        }

        public ActionResult LoginOut()
        {
            HttpContext.Authentication.SignOutAsync("MyCookies");
            return RedirectToRoute(new { controller = "Login", action = "Login", culture = HttpContext.Session.GetString("culture") });
        }
    }
}
