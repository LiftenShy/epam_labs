﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using PagedList.Core;
using Tickets.Business.Abstract;
using Tickets.Data.Models;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class EventsController : Controller
    {
        public IEventsService EventService { get; set; } 
        public IStringLocalizer<BaseLocalizationController> StringLocalizer { get; set; }

        public EventsController(IEventsService eventsService, IStringLocalizer<BaseLocalizationController> stringLocalizer)
        {
            EventService = eventsService;
            StringLocalizer = stringLocalizer;
        }

        
        [HttpGet]
        [AllowAnonymous]
        [Authorize(Roles = "user, admin")]
        public ActionResult Events(int? page)
        {
            return View("Events", new EventModel { ListEvents = EventService.GetAllEvents() });
        }
    }
}