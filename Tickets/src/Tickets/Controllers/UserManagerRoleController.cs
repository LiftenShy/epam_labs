﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Tickets.Business.Abstract;
using Tickets.Data.Models;
using Tickets.Models;


namespace Tickets.Controllers
{
    [Authorize(Roles = "admin")]
    public class UserManagerRoleController : Controller
    {
        public IUserService UserService { get; set; }
        public IStringLocalizer<BaseLocalizationController> StringLocalizer { get; set; }

        public UserManagerRoleController(IUserService userService, IStringLocalizer<BaseLocalizationController> stringLocalizer)
        {
            UserService = userService;
            StringLocalizer = stringLocalizer;
        }

        [HttpGet]
        
        public ActionResult UsersInRole()
        {
            return View("UserInRole",new UserInRoleModel { ListUserAccount = UserService.GetAllUserAccounts()});
        }

        public ActionResult ChangeRole(int id)
        {
            return View("ChangeRoleForuser",new UserInRoleModel {  UserAccount = UserService.GetUserAccountByid(id)});
        }

        [HttpPost]
        public ActionResult ChangeRolePost(UserInRoleModel userAccount)
        {
            try
            {
                UserService.ChangeRoleByUser(userAccount.NewRole, userAccount.Id);
                return RedirectToAction("UsersInRole");
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError("", StringLocalizer[e.Message]);
                return View("ChangeRoleForuser", userAccount);
            }
        }
    }
}
