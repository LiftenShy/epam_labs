﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Tickets.Business.Abstract;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class UserOrdersController : Controller
    {
        public IOrderService OrderService { get; set; }
        public IUserService UserService { get; set; }
        public IStringLocalizer<BaseLocalizationController> StringLocalizer { get; set; }

        public UserOrdersController(IOrderService orderService,IUserService userService, IStringLocalizer<BaseLocalizationController> stringLocalizer)
        {
            OrderService = orderService;
            UserService = userService;
            StringLocalizer = stringLocalizer;
        }

        public ActionResult UserOrders()
        {
            return View("UserOrders", new UserOrdersModel { ListOrders = OrderService.GetOrdersByName(User.Identity.Name) });
        }
    }
}
