﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Tickets.Business.Abstract;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class UserTicketsController : Controller
    {
        public ITicketsService MyTickets { get; set; }
        public IStringLocalizer<BaseLocalizationController> StringLocalizer { get; set; }

        public UserTicketsController(ITicketsService myTickets, IStringLocalizer<BaseLocalizationController> stringLocalizer)
        {
            MyTickets = myTickets;
            StringLocalizer = stringLocalizer;
        }

        [Authorize(Roles = "user, admin")]
        public ActionResult UserTickets()
        {
            return View("UserTickets", new UserTicketModel { ListTickets = MyTickets.GetTicketsByUserName(User.Identity.Name) });
        }
    }
}
