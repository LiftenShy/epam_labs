﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Tickets.Business.Abstract;
using Tickets.Data.Models;
using Tickets.Models;


namespace Tickets.Controllers
{
    public class TicketsController : Controller
    {
        private readonly IEventsService _eventsService;
        private readonly IUserService _userService;
        private readonly IOrderService _orderService;
        private readonly ITicketsService _ticketsService;
        public IStringLocalizer<BaseLocalizationController> StringLocalizer { get; set; }

        public TicketsController(ITicketsService ticketsService,
            IEventsService eventsService,
            IStringLocalizer<BaseLocalizationController> stringLocalizer,
            IUserService userService,
            IOrderService orderService)
        {
            StringLocalizer = stringLocalizer;
            _eventsService = eventsService;
            _userService = userService;
            _orderService = orderService;
            _ticketsService = ticketsService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Authorize(Roles = "user, admin")]
        public IActionResult TicketsPage(int id)
        {
            return View("Tickets",
                new TicketModel
                {
                    Event = _eventsService.GetEventById(id),
                    ListTickets = _ticketsService.GetTicketsByIdEvent(id)
                });
        }

        public IActionResult AddTicket(int id)
        {
            return View("AddTicket", new TicketModel {IdEvent = id});
        }

        [HttpPost]
        public IActionResult AddTicketPost(TicketModel ticketModel)
        {
            _ticketsService.AddTicket(new Ticket
            {
                Seller = User.Identity.Name,
                Price = ticketModel.Price,
                EventId = ticketModel.IdEvent,
                Status = "Selling",
                UserId = _userService.GetUserByName(User.Identity.Name).Id
            });
            return RedirectToRoute(
                new
                {
                    controller = "UserTickets",
                    action = "UserTickets",
                    culture = HttpContext.Session.GetString("culture")
                });
        }

        public IActionResult AcceptBuyTicket(int id)
        {
            return View("AcceptTicket", new TicketModel {Ticket = _ticketsService.GetTicketById(id)});
        }

        public IActionResult BuyTicket(int id)
        {
            _ticketsService.BuyTicket(_ticketsService.GetTicketById(id));
            _orderService.SetOrderToUser(_orderService.GetOrderById(id),_userService.GetUserAccountByName(User.Identity.Name));
            return RedirectToRoute(
                new
                {
                    controller = "UserOrders",
                    action = "UserOrders",
                    culture = HttpContext.Session.GetString("culture")
                });
        }

        public IActionResult AcceptTicket(int id)
        {
            _ticketsService.Accept(_ticketsService.GetTicketById(id));
            _orderService.Accept(_orderService.GetOrderById(id));
            return RedirectToRoute(
                new
                {
                    controller = "UserTickets",
                    action = "UserTickets",
                    culture = HttpContext.Session.GetString("culture")
                });
        }

        public IActionResult RejectTicket(int id)
        {
            return View("../UserTickets/RejectTicket", new UserTicketModel{Ticket = _ticketsService.GetTicketById(id)});
        }

        [HttpPost]
        public IActionResult RejectTicketPost(UserTicketModel model)
        {
            try
            {
                _ticketsService.Reject(_ticketsService.GetTicketById(model.TicketId));
                _orderService.Reject(_orderService.GetOrderById(model.TicketId), model.Discription);
                return RedirectToRoute(
                    new
                    {
                        controller = "UserTickets",
                        action = "UserTickets",
                        culture = HttpContext.Session.GetString("culture")
                    });
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError("", StringLocalizer[e.Message]);
                return RedirectToRoute(
                    new
                    {
                        controller = "UserTicket",
                        action = "UserTickets",
                        culture = HttpContext.Session.GetString("culture")
                    });
            }
        }
    }
}
