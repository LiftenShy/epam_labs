﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Tickets.Controllers
{
    public class BaseLocalizationController : Controller
    {
        public ActionResult ChooseLanguage(string culture)
        {
            if (culture.Equals("en"))
            {
                HttpContext.Session.SetString("culture", "en");
            }
            if (culture.Equals("ru"))
            {
                HttpContext.Session.SetString("culture", "ru");
            }
            if (culture.Equals("be"))
            {
                HttpContext.Session.SetString("culture", "be");
            }
            return Redirect("~/Events/Events?culture=" + HttpContext.Session.GetString("culture"));
        }
    }
}
