﻿using System.Collections.Generic;
using Tickets.Data.Models;

namespace Tickets.Models
{
    public class UserOrdersModel
    {
        public List<Order> ListOrders { get; set; }
    }

}