﻿using System.ComponentModel.DataAnnotations;


namespace Tickets.Models
{
    public class UsersModel
    {
        public enum Role {User = 1, Admin = 2 }
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
