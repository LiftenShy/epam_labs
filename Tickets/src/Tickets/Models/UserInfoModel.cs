﻿using Tickets.Data.Models;

namespace Tickets.Models
{
    public class UserInfoModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Address { get; set; }
        public int PhoneNumber { get; set; }

        public User User { get; set; }
    }
}
