﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Data.Models;

namespace Tickets.Models
{
    public class UserInRoleModel
    {
        public string NewRole { get; set; }
        public int Id { get; set; }

        public UserAccount UserAccount { get; set; }
        public List<UserAccount> ListUserAccount { get; set; }
    }
}
