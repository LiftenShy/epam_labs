﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Data.Models;

namespace Tickets.Models
{
    public class VenueModel
    {
        public string Name { get; set; }
        public string Adress { get; set; }
        public string NameCity { get; set; }

        public List<City> ListCity { get; set; }
    }
}
