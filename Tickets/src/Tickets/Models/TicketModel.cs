﻿using System.Collections.Generic;
using Tickets.Data.Models;

namespace Tickets.Models
{
    public class TicketModel
    {
        public decimal Price { get; set; }
        public string Seller { get; set; }
        public string Status { get; set; }

        public int IdTicket { get; set; }
        public int IdEvent { get; set; }

        public Event Event { get; set; }
        public List<Ticket> ListTickets { get; set; }
        public Ticket Ticket { get; set; }
    }
}
