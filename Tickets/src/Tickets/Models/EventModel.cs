﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using Microsoft.AspNetCore.Http;
using Tickets.Data.Models;

namespace Tickets.Models
{
    public class EventModel
    {
        public string Name { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{dd.MM}")]
        public DateTime Date { get; set; }
        public IFormFile Image { get; set; }
        public string Description { get; set; }

        public string CityName { get; set; }

        public string VenueName { get; set; }

        public List<City> ListCity { get; set; }

        public List<Venue> ListVenue { get; set; }


        public Event Event { get; set; }

        public List<Event> ListEvents { get; set; }
    }
}
