﻿using System.Collections.Generic;
using Tickets.Data.Models;

namespace Tickets.Models
{
    public class UserTicketModel
    {
        public List<Ticket> ListTickets { get; set; }
        public Ticket Ticket { get; set; }
        public string Discription { get; set; }
        public int TicketId { get; set; }
    }
}
