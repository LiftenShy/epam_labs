﻿using System;

namespace Tickets.Data.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string NameBuyer { get; set; }
        public int BuyerId { get; set; }
        public Guid TackNo { get; set; }
        public string Description { get; set; }

        public int TicketId { get; set; }
        public Ticket Ticket { get; set; }
    }
}
