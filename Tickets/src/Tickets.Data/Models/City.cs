﻿
using System.Collections.Generic;

namespace Tickets.Data.Models
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Venue> Venues { get; set; }
    }
}
