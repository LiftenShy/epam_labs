﻿using System;
using System.Collections.Generic;

namespace Tickets.Data.Models
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string PathBanner { get; set; }
        public string Description { get; set; }

        public virtual List<Ticket> Tickets { get; set; }

        public int VenueId { get; set; }
        public virtual Venue Venue { get; set; }
    }
}
