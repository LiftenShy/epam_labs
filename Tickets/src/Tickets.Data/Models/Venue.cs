﻿using System.Collections.Generic;

namespace Tickets.Data.Models
{
    public class Venue
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }

        public int CityId { get; set; }
        public virtual City City { get; set; }

        public virtual List<Event> Events { get; set; }
    }
}
