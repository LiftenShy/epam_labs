﻿using System.Collections.Generic;

namespace Tickets.Data.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string NameRole { get; set; }

        public virtual List<UserAccount> UserAccounts { get; set; }
    }
}
