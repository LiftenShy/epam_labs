﻿
using System.Collections.Generic;

namespace Tickets.Data.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Address { get; set; }
        public int PhoneNumber { get; set; }

        public UserAccount UserAccount { get; set;}

        public virtual List<Ticket> Tickets { get; set; }
    }
}
