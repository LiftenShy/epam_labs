﻿
namespace Tickets.Data.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string Seller { get; set; }
        public string Status { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
        
        public int EventId { get; set; }
        public virtual Event Event { get; set; }

        public Order Order { get; set; }
    }
}
