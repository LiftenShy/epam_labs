﻿using System;
using System.Collections.Generic;
using Tickets.Data.Models;

namespace Tickets.Data
{
    public class OnStartAppFirstData
    {
        public List<Order> _listOrders = new List<Order>
        {
            new Order {NameBuyer = "", Status = "", TackNo = Guid.NewGuid(), BuyerId = 0, TicketId = 1},
            new Order
            {
                NameBuyer = "admin",
                Status = "Waiting for confirmation",
                TackNo = Guid.NewGuid(),
                BuyerId = 2,
                TicketId = 2
            },
            new Order {NameBuyer = "user", Status = "Confirmed", TackNo = Guid.NewGuid(), BuyerId = 1, TicketId = 3},
            new Order
            {
                NameBuyer = "user",
                Status = "Rejected",
                Description = "negative recall",
                TackNo = Guid.NewGuid(),
                BuyerId = 1,
                TicketId = 4
            },
            new Order
            {
                NameBuyer = "sergey",
                Status = "Waiting for confirmation",
                TackNo = Guid.NewGuid(),
                BuyerId = 3,
                TicketId = 5
            },
            new Order {NameBuyer = "sergey", Status = "Confirmed", TackNo = Guid.NewGuid(), BuyerId = 3, TicketId = 6},
            new Order
            {
                NameBuyer = "user",
                Status = "Waiting Confirmation",
                TackNo = Guid.NewGuid(),
                BuyerId = 1,
                TicketId = 7
            },
            new Order
            {
                NameBuyer = "sergey",
                Status = "Waiting for confirmation",
                TackNo = Guid.NewGuid(),
                BuyerId = 3,
                TicketId = 8
            },
            new Order {NameBuyer = "sergey", Status = "Confirmed", TackNo = Guid.NewGuid(), BuyerId = 3, TicketId = 9},
            new Order {NameBuyer = "", Status = "", TackNo = Guid.NewGuid(), BuyerId = 0, TicketId = 10}
        };

        public List<Event> _listEvents = new List<Event>
        {
            new Event
            {
                Name = "concert popular rock group",
                Date = DateTime.Today,
                Description = "Rock group Skorpion",
                PathBanner = @"~/Images/event1.jpg",
                VenueId = 1
            },
            new Event
            {
                Name = "Exhibition of paintings in gallery",
                Date = DateTime.Today,
                Description = "Paintings Picaso",
                PathBanner = @"~/Images/event2.jpg",
                VenueId = 2
            }
        };

        public List<City> _listCity = new List<City>
        {
            new City {Name = "Gomel"},
            new City {Name = "San-Martin"},
            new City {Name = "Minsk"},
            new City {Name = "Abu-Dabi"},
            new City {Name = "Nara"},
            new City {Name = "Hartum"},
        };

        public List<Venue> _listVenue = new List<Venue>
        {
            new Venue
            {
                Name = "Dinamo Misnk",
                Adress = "1941 W. Augusta Blvd.",
                CityId = 1
            },
            new Venue
            {
                Name = "Park vine",
                Adress = "1219-1223 Vine Street",
                CityId = 2
            },
            new Venue
            {
                Name = "Museu weaver",
                Adress = "1250 Weaver Street,",
                CityId = 3
            },
            new Venue
            {
                Name = "Bank folsom",
                Adress = "1072 Folsom Street, #470",
                CityId = 4
            },
            new Venue
            {
                Name = "Theatr spandia",
                Adress = "174 Spandia Avenue, Suite 610",
                CityId = 5
            },
            new Venue {Name = "Club po", Adress = "PO Box 86605", CityId = 6},
            new Venue {Name = "Cinema aiso", Adress = "120 North Judge John Aiso Street", CityId = 1},
            new Venue {Name = "Club ave", Adress = "977 S Van Ness Ave,", CityId = 2},
            new Venue {Name = "Arena suite", Adress = "109 West 27th Street, Suite 9A", CityId = 3},
            new Venue {Name = "Catsle azarova", Adress = "410 Queens Quay West", CityId = 4},
            new Venue {Name = "Square loskova", Adress = "1145 Wilshire Boulevard, Suite 100-D", CityId = 5},
            new Venue {Name = "House l", Adress = "Alissa L. Alcosiba", CityId = 6},
        };

        public List<User> _listUsers = new List<User>
        {
            new User
            {
                Address = "Pushkina 3",
                FirstName = "Siri",
                LastName = "Microsoft",
                Localization = "en",
                PhoneNumber = 3454643
            },
            new User
            {
                Address = "Gagarina 10",
                FirstName = "Sergey",
                LastName = "Krestovec",
                Localization = "en",
                PhoneNumber = 3451253
            },
            new User
            {
                Address = "Barisa-Carikova 12",
                FirstName = "Jon",
                LastName = "Brayec",
                Localization = "en",
                PhoneNumber = 8941303

            }
        };

        public List<Ticket> _listTickets = new List<Ticket>
        {
            new Ticket
            {
                Seller = "user",
                Price = 12300,
                Status = "Selling",
                EventId = 1,
                UserId = 1
            },
            new Ticket
            {
                Seller = "user",
                Price = 9300,
                Status = "Waiting Confirmation",
                EventId = 1,
                UserId = 1
            },
            new Ticket
            {
                Seller = "admin",
                Price = 15300,
                Status = "Sold",
                EventId = 1,
                UserId = 2
            },
            new Ticket
            {
                Seller = "admin",
                Price = 10300,
                Status = "Sold",
                EventId = 1,
                UserId = 2
            },
            new Ticket
            {
                Seller = "admin",
                Price = 14300,
                Status = "Waiting Confirmation",
                EventId = 1,
                UserId = 2
            },
            new Ticket
            {
                Seller = "admin",
                Price = 13300,
                Status = "Sold",
                EventId = 2,
                UserId = 2
            },
            new Ticket
            {
                Seller = "admin",
                Price = 11300,
                Status = "Waiting Confirmation",
                EventId = 2,
                UserId = 2
            },
            new Ticket
            {
                Seller = "admin",
                Price = 10520,
                Status = "Waiting Confirmation",
                EventId = 2,
                UserId = 2
            },
            new Ticket
            {
                Seller = "admin",
                Price = 12000,
                Status = "Selling",
                EventId = 2,
                UserId = 2
            },
            new Ticket
            {
                Seller = "admin",
                Price = 11450,
                Status = "Sold",
                EventId = 2,
                UserId = 2
            }
        };

        public List<Role> _listRoles = new List<Role>
        {
            new Role {NameRole = "user"},
            new Role {NameRole = "admin"},
        };

        public List<UserAccount> _listAccountUsers = new List<UserAccount>
        {
            new UserAccount
            {
                UserName = "user",
                Password = "user",
                RoleId = 1,
                UserId = 1
            },
            new UserAccount
            {
                UserName = "sergey",
                Password = "qaz",
                RoleId = 1,
                UserId = 3
            },
            new UserAccount {UserName = "admin", Password = "admin", UserId = 2, RoleId = 2}
        };
    }
}
