﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Tickets.Data.Models;

namespace Tickets.Data
{
    public class TicketContext : DbContext
    {
        public DbSet<Event> Events { get; set; }
        public DbSet<City> Citys { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server = (localdb)\mssqllocaldb; Database = ResaleDB_Sergey_Privalau; Trusted_Connection = True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Event>()
                .HasOne(e => e.Venue)
                .WithMany(v => v.Events)
                .HasForeignKey(v => v.VenueId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Venue>()
                .HasOne(v => v.City)
                .WithMany(c => c.Venues)
                .HasForeignKey(v => v.CityId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Ticket>()
                .HasOne(t => t.Event)
                .WithMany(e => e.Tickets)
                .HasForeignKey(t => t.EventId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Ticket>()
                .HasOne(t => t.User)
                .WithMany(u => u.Tickets)
                .HasForeignKey(t => t.UserId);

            modelBuilder.Entity<Ticket>()
                .HasOne(t => t.Order)
                .WithOne(o => o.Ticket)
                .HasForeignKey<Order>(o => o.TicketId);

            modelBuilder.Entity<User>()
                .HasOne(ua => ua.UserAccount)
                .WithOne(ua => ua.User)
                .HasForeignKey<UserAccount>(ua => ua.UserId);

            modelBuilder.Entity<UserAccount>()
                .HasOne(ua => ua.Role)
                .WithMany(r => r.UserAccounts)
                .HasForeignKey(ua => ua.RoleId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
