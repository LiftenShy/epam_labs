﻿using System;
using System.Data.Common;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Tickets.Data.Models;

namespace Tickets.Data
{
    public class EfRepository<T> : IRepository<T> where T : class 
    {
        private readonly TicketContext _context = new TicketContext();
        private DbSet<T> _entities;

        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            Entities.Add(entity);
            _context.SaveChanges();
        }

        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            //Entities.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }

                Entities.Remove(entity);
                _context.SaveChanges();
            }
            catch (DbException ex)
            {
                throw ex.InnerException;
            }
        }

        public virtual IQueryable<T> Table => this.Entities;

        private DbSet<T> Entities => _entities ?? (_entities = _context.Set<T>());

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}
