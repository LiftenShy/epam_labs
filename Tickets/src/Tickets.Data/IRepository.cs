using System;
using System.Linq;
using Tickets.Data.Models;

namespace Tickets.Data
{
    public interface IRepository<T> :IDisposable where T : class
    {
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
        IQueryable<T> Table { get; }
    }
}