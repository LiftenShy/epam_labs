﻿using System.Windows;
using Catel.Data;
using Performance_Testing.Models;
using Performance_Testing.Services;
using Performance_Testing.Services.Interfaces;

namespace Performance_Testing.ViewModels
{
    using Catel.MVVM;
    using System.Threading.Tasks;
    using Unity.Attributes;

    public class ProgressWindowViewModel : ViewModelBase
    {
        [Dependency]
        public ITestService TestService { get; set; }

        [Dependency]
        public IPdfDocumentService PdfDocumentService { get; set; }

        public ProgressWindowViewModel(Parameter parameter)
        {
            Statistics = new Statistics
            {
                Parameter = parameter,
                MinResponse = 100000,
                PrintPdfButtonVisibility = Visibility.Hidden
            };
            TestService = new TestService();
            PdfDocumentService = new PdfDocumentService();
        }

        public Statistics Statistics
        {
            get { return GetValue<Statistics>(StastisticProperty); }
            set { SetValue(StastisticProperty, value); }
        }

        public static readonly PropertyData StastisticProperty = RegisterProperty("Statistics", typeof(Statistics));

        public override string Title => "Statistic";

        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();
            TestService.RunTest(Statistics);
        }

        protected override async Task CloseAsync()
        {
            await base.CloseAsync();
        }

        public Command PrintResultIntoPdfDocumentCommand
        {
            get
            {
                return new Command(() =>
                {
                    PdfDocumentService.SaveResult(Statistics);
                    MessageBox.Show("Result was print");
                });
            }
        }

        public Command CancelTestCommand
        {
            get
            {
                return new Command(() =>
                {
                    TestService.CancelTest();
                    MessageBox.Show("Test cancel");
                });
            }
        }
    }
}
