﻿using Catel.Data;
using Catel.Services;
using Performance_Testing.Models;

namespace Performance_Testing.ViewModels
{
    using Catel.MVVM;
    using System.Threading.Tasks;

    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IUIVisualizerService _uiVisualizerService;

        public MainWindowViewModel(IUIVisualizerService uiVisualizerService)
        {
            _uiVisualizerService = uiVisualizerService;
        }

        public override string Title => "Parameters";


        protected override async Task InitializeAsync()
        {
            Parameter = new Parameter();
            await base.InitializeAsync();
        }

        protected override async Task CloseAsync()
        {
            await base.CloseAsync();
        }

        public Parameter Parameter
        {
            get { return GetValue<Parameter>(ParameterProperty); }
            set { SetValue(ParameterProperty, value); }
        }

        public static readonly PropertyData ParameterProperty = RegisterProperty("Parameter", typeof(Parameter));

        public Command Run
        {
            get
            {
                return new Command(() =>
                {
                    if (Parameter.IsReady)
                    {
                        _uiVisualizerService.ShowAsync(new ProgressWindowViewModel(Parameter));
                    }
                });
            }
        }
    }
}
