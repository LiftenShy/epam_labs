﻿
using Performance_Testing.Models;

namespace Performance_Testing.Services.Interfaces
{
    public interface ITestService
    {
        void RunTest(Statistics statistics);
        void CancelTest();
    }
}
