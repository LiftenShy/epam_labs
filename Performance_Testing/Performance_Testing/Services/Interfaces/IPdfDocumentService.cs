﻿
using Performance_Testing.Models;

namespace Performance_Testing.Services.Interfaces
{
    public interface IPdfDocumentService
    {
        void SaveResult(Statistics statistics);
    }
}
