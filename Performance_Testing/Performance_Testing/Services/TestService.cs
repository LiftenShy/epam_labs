﻿
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Performance_Testing.Models;
using Performance_Testing.Services.Interfaces;

namespace Performance_Testing.Services
{
    public class TestService : ITestService
    {
        CancellationTokenSource _cts = new CancellationTokenSource();
        private bool _isOver = false;
        private bool _IsComplete = false;
        public void RunTest(Statistics statistics)
        {
            for (int i = 0; i < statistics.Parameter.CountThread; i++)
            {
                RealTimeTest(statistics, _cts.Token);
            }

            DispatcherTimer dispatcherTimer2 = new DispatcherTimer();
            int time = 1;
            dispatcherTimer2.Tick += (sender, e) =>
            {
                if (time > statistics.Parameter.Duration)
                {
                    _IsComplete = true;
                    dispatcherTimer2.Stop();
                    lock (statistics)
                    {
                        statistics.AverageResponse = (statistics.MaxResponce + statistics.MinResponse) / 2;
                        statistics.TotalRequests = statistics.Parameter.ReqPerSecond * statistics.Parameter.CountThread *
                                                   statistics.Parameter.Duration;
                        statistics.BandwidthMbits = TakeBandwidthMbits(statistics.ContentLenth, statistics.Time);
                        statistics.PrintPdfButtonVisibility = Visibility.Visible;
                    }
                }
                else
                {
                    time = TakeData(sender, e, statistics, time);
                }
            };
            dispatcherTimer2.Interval = new TimeSpan(0, 0, 0, 1);
            dispatcherTimer2.Start();
        }

        private void RealTimeTest(Statistics statistics, CancellationToken token)
        {
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += (sender, e) =>
            {
                if (_isOver || _IsComplete)
                {
                    dispatcherTimer.Stop();
                    lock (statistics)
                    {
                        statistics.ProgressBarValue += 100 / statistics.Parameter.CountThread;
                    }
                }
                else
                {
                    SendRequests(sender, e, statistics, token);
                }
            };
            dispatcherTimer.Interval = TimeSpan.FromSeconds(1000 / statistics.Parameter.ReqPerSecond);
            dispatcherTimer.Start();
        }

        /// <summary>
        /// Convert long64 to Mbit and divide on time send and take request
        /// </summary>
        /// <param name="value"></param>
        /// <param name="time"></param>
        /// <param name="decimalPlaces"></param>
        /// <returns>Decimal</returns>
        private decimal TakeBandwidthMbits(decimal value, long time)
        {
            return ((value / 2048) / (time / 100));
        }

        private int TakeData(object sender, EventArgs e, Statistics statistics, int time)
        {
            statistics.TotalRequestPerSecond /= time;
            statistics.SuccessfulRequestsPerSecond /= time;
            statistics.FailedRequestsPerSecond /= time;
            time++;
            return time;
        }

        private async void SendRequests(object sender, EventArgs e, Statistics statistics, CancellationToken  token)
        {
            Stopwatch sw = new Stopwatch();
            using (var client = new HttpClient())
            {
                try
                {
                    sw.Start();
                    HttpResponseMessage response = await client.GetAsync(statistics.Parameter.Url, token);
                    sw.Stop();
                    lock (statistics)
                    {
                        statistics.TotalRequestPerSecond++;
                        statistics.ContentLenth = (decimal)response.Content.Headers.ContentLength;
                        statistics.Time = sw.ElapsedMilliseconds;
                    }
                    if (sw.ElapsedMilliseconds < statistics.MinResponse)
                    {
                        lock (statistics)
                        {
                            statistics.MinResponse = sw.ElapsedMilliseconds;
                        }
                    }
                    if (sw.ElapsedMilliseconds > statistics.MaxResponce)
                    {
                        lock (statistics)
                        {
                            statistics.MaxResponce = sw.ElapsedMilliseconds;
                        }
                    }
                    if (response.IsSuccessStatusCode)
                    {
                        lock (statistics)
                        {
                            statistics.SuccessfulRequestsPerSecond++;
                        }
                    }
                    else
                    {
                        lock (statistics)
                        {
                            statistics.FailedRequestsPerSecond++;
                        }
                    }
                }
                catch
                {
                    lock (statistics)
                    {
                        statistics.TotalErrors++;
                    }
                }
            }
        }

        public void CancelTest()
        {
            _cts.Cancel();
            _isOver = true;
        }
    }
}
