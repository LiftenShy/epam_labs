﻿using System.Globalization;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Performance_Testing.Models;
using Performance_Testing.Services.Interfaces;

namespace Performance_Testing.Services
{
    public class PdfDocumentService : IPdfDocumentService
    {
        public void SaveResult(Statistics statistics)
        {
            using (var document = new Document())
            {
                PdfWriter.GetInstance(document, new StreamWriter("result.pdf").BaseStream);
                document.Open();
                document.Add(new Paragraph("-----------------Request info----------------------"));
                document.Add(new Paragraph($"Total request per second: {(float)statistics.TotalRequestPerSecond}"));
                document.Add(new Paragraph($"Successful request per second: {(float)statistics.SuccessfulRequestsPerSecond}"));
                document.Add(new Paragraph($"Failed request per second: {(float)statistics.FailedRequestsPerSecond}"));
                document.Add(new Paragraph("----------------Settings info--------------"));
                document.Add(new Paragraph($"Total request: {statistics.TotalRequests}"));
                document.Add(new Paragraph($"Count thread: {statistics.Parameter.CountThread}"));
                document.Add(new Paragraph($"Count request per second: {statistics.Parameter.ReqPerSecond}"));
                document.Add(new Paragraph($"Duration test: {statistics.Parameter.Duration}"));
                document.Add(new Paragraph($"Url: {statistics.Parameter.Url}"));
                document.Add(new Paragraph($"Total Errors: {statistics.TotalErrors} "));
                document.Add(new Paragraph($"Bandwidth Mbit/s: {statistics.BandwidthMbits} "));
                document.Add(new Paragraph("----------------Responce info------------------------"));
                document.Add(new Paragraph($"Max responce: {statistics.MaxResponce.ToString(CultureInfo.InvariantCulture)}"));
                document.Add(new Paragraph($"Min responce: {statistics.MinResponse.ToString(CultureInfo.InvariantCulture)}"));
                document.Add(new Paragraph($"Average responce: {statistics.AverageResponse.ToString(CultureInfo.InvariantCulture)}"));
                document.Close();
            }
        }
    }
}
