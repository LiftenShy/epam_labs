﻿using System.Windows;
using Unity;

namespace Performance_Testing
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            IUnityContainer container = new UnityContainer();
            var mainWindow = container.Resolve<Views.MainWindow>();
            Current.MainWindow = mainWindow;
            Current.MainWindow.Show();
        }
    }
}
