﻿using System;
using System.Collections.Generic;
using System.Windows;
using Catel.Data;
using System.Net.Http;

namespace Performance_Testing.Models
{
    class ParametersValidator : ValidatorBase<Parameter>
    {
        protected override void ValidateFields(Parameter instance, List<IFieldValidationResult> validationResults)
        {

            if (instance.CountThread < 0)
            {
                validationResults.Add(FieldValidationResult.CreateError(Parameter.CountThreadProperty,
                    "Number must be positive"));
            }
            if (!string.IsNullOrEmpty(instance.Url) && !string.IsNullOrWhiteSpace(instance.Url))
            {
                CheckUrl(instance, validationResults);
            }
            else
            {
                validationResults.Add(FieldValidationResult.CreateError(Parameter.UrlProperty, "Field empty"));
                instance.IsReady = false;
            }
            if (instance.ReqPerSecond < 0)
            {
                validationResults.Add(FieldValidationResult.CreateError(Parameter.ReqPerSecondProperty,
                    "Number must be positive"));
            }
            if (instance.Duration < 0)
            {
                validationResults.Add(FieldValidationResult.CreateError(Parameter.DurationProperty,
                    "Number must be positive"));
            }
        }

        public async void CheckUrl(Parameter instance, List<IFieldValidationResult> validationResults)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var response = await client.GetAsync(instance.Url);
                    if (response.IsSuccessStatusCode)
                    {
                        instance.IsReady = true;
                    }
                    else
                    {
                        MessageBox.Show($"Enter url: {response.StatusCode}", "Trouble with connection",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                        instance.IsReady = false;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show($"Incorrect url adress! [detail error: {e.Message} ]", "Incorrect url adress",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    instance.IsReady = false;
                }
            }
        }
    }
}
