﻿using Catel.Data;

namespace Performance_Testing.Models
{
    [ValidateModel(typeof(ParametersValidator))]
    public class Parameter : ModelBase
    {
        /// <summary>
            /// Gets or sets the property value.
            /// </summary>
        public int CountThread
        {
            get { return GetValue<int>(CountThreadProperty); }
            set { SetValue(CountThreadProperty, value); }
        }

        /// <summary>
        /// Register the CountThread property so it is known in the class.
        /// </summary>
        public static readonly PropertyData CountThreadProperty = RegisterProperty("CountThread", typeof(int));

        /// <summary>
            /// Gets or sets the property value.
            /// </summary>
        public int ReqPerSecond
        {
            get { return GetValue<int>(ReqPerSecondProperty); }
            set { SetValue(ReqPerSecondProperty, value); }
        }

        /// <summary>
        /// Register the ReqPerSecond property so it is known in the class.
        /// </summary>
        public static readonly PropertyData ReqPerSecondProperty = RegisterProperty("ReqPerSecond", typeof(int));

        /// <summary>
            /// Gets or sets the property value.
            /// </summary>
        public string Url
        {
            get { return GetValue<string>(UrlProperty); }
            set { SetValue(UrlProperty, value); }
        }

        /// <summary>
        /// Register the Url property so it is known in the class.
        /// </summary>
        public static readonly PropertyData UrlProperty = RegisterProperty("Url", typeof(string));

        /// <summary>
            /// Gets or sets the property value.
            /// </summary>
        public int Duration
        {
            get { return GetValue<int>(DurationProperty); }
            set { SetValue(DurationProperty, value); }
        }

        /// <summary>
        /// Register the Duration property so it is known in the class.
        /// </summary>
        public static readonly PropertyData DurationProperty = RegisterProperty("Duration", typeof(int));

        /// <summary>
            /// Gets or sets the property value.
            /// </summary>
        public bool IsReady
        {
            get { return GetValue<bool>(IsReadyProperty); }
            set { SetValue(IsReadyProperty, value); }
        }

        /// <summary>
        /// Register the IsReady property so it is known in the class.
        /// </summary>
        public static readonly PropertyData IsReadyProperty = RegisterProperty("IsReady", typeof(bool));
    }
}
