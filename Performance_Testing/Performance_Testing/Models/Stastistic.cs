﻿using System.Windows;
using Catel.Data;

namespace Performance_Testing.Models
{
    public class Statistics : ModelBase
    {
        public Parameter Parameter
        {
            get { return GetValue<Parameter>(ParameterProperty); }
            set { SetValue(ParameterProperty, value); }
        }

        public double FailedRequestsPerSecond
        {
            get { return GetValue<double>(FailedRequestsPerSecondProperty); }
            set { SetValue(FailedRequestsPerSecondProperty, value); }
        }

        public static readonly PropertyData FailedRequestsPerSecondProperty = RegisterProperty("FailedRequestsPerSecond", typeof(double));

        public double TotalRequestPerSecond
        {
            get { return GetValue<double>(TotalRequestPerSecondProperty); }
            set { SetValue(TotalRequestPerSecondProperty, value); }
        }

        public static readonly PropertyData TotalRequestPerSecondProperty = RegisterProperty("TotalRequestPerSecond", typeof(double));

        public double SuccessfulRequestsPerSecond
        {
            get { return GetValue<double>(SuccessfulRequestsPerSecondProperty); }
            set { SetValue(SuccessfulRequestsPerSecondProperty, value); }
        }

        public static readonly PropertyData SuccessfulRequestsPerSecondProperty = RegisterProperty("SuccessfulRequestsPerSecond", typeof(double));

        public long Time
        {
            get { return GetValue<long>(TimeProperty); }
            set { SetValue(TimeProperty, value); }
        }

        public static readonly PropertyData TimeProperty = RegisterProperty("Time", typeof(long));

        public static readonly PropertyData ParameterProperty = RegisterProperty("Parameter", typeof(Parameter));

        public int TotalRequests
        {
            get { return GetValue<int>(TotalRequestsProperty); }
            set { SetValue(TotalRequestsProperty, value); }
        }

        public static readonly PropertyData TotalRequestsProperty = RegisterProperty("TotalRequests", typeof(int));

        public decimal BandwidthMbits
        {
            get { return GetValue<decimal>(BandwidthMbitsProperty); }
            set { SetValue(BandwidthMbitsProperty, value); }
        }

        public static readonly PropertyData BandwidthMbitsProperty = RegisterProperty("BandwidthMbits", typeof(decimal));

        public int TotalErrors
        {
            get { return GetValue<int>(TotalErrorsProperty); }
            set { SetValue(TotalErrorsProperty, value); }
        }

        public static readonly PropertyData TotalErrorsProperty = RegisterProperty("TotalErrors", typeof(int));

        public double MinResponse
        {
            get { return GetValue<double>(MinResponseProperty); }
            set { SetValue(MinResponseProperty, value); }
        }

        public static readonly PropertyData MinResponseProperty = RegisterProperty("MinResponse", typeof(double));

        public double MaxResponce
        {
            get { return GetValue<double>(MaxResponceProperty); }
            set { SetValue(MaxResponceProperty, value); }
        }

        public static readonly PropertyData MaxResponceProperty = RegisterProperty("MaxResponce", typeof(double));

        public double AverageResponse
        {
            get { return GetValue<double>(AverageResponseProperty); }
            set { SetValue(AverageResponseProperty, value); }
        }

        public static readonly PropertyData AverageResponseProperty = RegisterProperty("AverageResponse", typeof(double));

        public int ProgressBarValue
        {
            get { return GetValue<int>(ProgressBarValueProperty); }
            set { SetValue(ProgressBarValueProperty, value); }
        }

        public static readonly PropertyData ProgressBarValueProperty = RegisterProperty("ProgressBarValue", typeof(int));

        public decimal ContentLenth
        {
            get { return GetValue<decimal>(ContentLenthProperty); }
            set { SetValue(ContentLenthProperty, value); }
        }

        public static readonly PropertyData ContentLenthProperty = RegisterProperty("ContentLenth", typeof(decimal));

        public Visibility PrintPdfButtonVisibility
        {
            get { return GetValue<Visibility>(IsCompleteProperty); }
            set { SetValue(IsCompleteProperty, value); }
        }

        public static readonly PropertyData IsCompleteProperty = RegisterProperty("PrintPdfButtonVisibility", typeof(Visibility));
    }
}
